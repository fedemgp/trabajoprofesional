#ifndef __APP_CONFIG_H__
#define __APP_CONFIG_H__

#include <QObject>
#include <QJsonObject>
#include <QFile>
#include <QTimer>
#include <string>
#include <map>
#include <vector>
#include <list>

#include "general_config.h"
#define APP_CONFIG_FILEPATH "app_configuration.json"

class ApplicationConfig: public QObject {
    Q_OBJECT
private:
    std::string filepath;
    QJsonObject json;
    std::map<QString, uint8_t> levelName2value;
    uint8_t log_level_enabled;

    ApplicationConfig(std::string filepath);
    void loadDefaultValues();
    void checkConsistency();
    void backupConfiguration();
    QJsonArray loglevel2array(uint8_t log_level);
    uint8_t array2LogLevel(const QJsonArray &array);
public:
    ~ApplicationConfig();
    static ApplicationConfig  &instance();
    uint8_t getWindowSize() const;
    uint8_t getLogLevel() const;
    double getCurrentAdjustmentCoefficient() const;
    void updateConfig(const GeneralConfig &conf);
    void saveControlConstant(float kp, float kd, float ki, const char* preset_name);
    void saveControlConstant(float kp, float kd, float ki, float kp_sensitivity, float kd_sensitivity,
                             float ki_sensitivity, float temp_error_sensitivy, float derivative_error_sensitivity,
                             const char *preset_name);
    void saveControlConstantSensitivity(float error_sensitivity, float derivative_error_sensitivity,
                                        float output_sensitivity, const char *preset_name);
    QJsonObject getControlConstants(const char *algorithm) const;
    std::list<QString> getPresetList() const;
    float getAntiwindup() const;
    float getIntegralActionLimit() const;

signals:
    void configChanged();
    void algorithmConstantChanged();
};

#endif
