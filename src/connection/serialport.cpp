//
// Created by Martín García on 18/09/2019.
//

#include "serialport.h"
#include "protocol/thermocouple_fault.h"
#include "protocol/power_set_acknowledge.h"
#include "messages.h"

#include <iostream>
#include <sstream>
#include <logger/logger.h>
#include <vector>
#include <connection/protocol/HeartBeat.h>

#define SEPARATOR ':'
#define BAUDRATE 115200
#define RECONNECTION_TIMEOUT 1000 // ms
#define HEARTBEAT_TIMEOUT 500 // ms
#define CONSECUTIVE_USB_ERROR_BEFORE_CLOSING_IT 3
#define PORT_SERIAL_NUMBER "12345679" // this is set up in firmware
#define PACKET_SIZE 0x08

SerialPort::SerialPort(QObject *parent) : QObject(parent), reconectionTimer(this),
                                          consecutiveErrors(0),
                                          heartbeatAcked(false),
                                          arrivingPacket(8,0) {
    this->serial_port = new QSerialPort(this);
    connect(&reconectionTimer, &QTimer::timeout, this, &SerialPort::findDevice);
    connect(this->serial_port, &QSerialPort::errorOccurred, this, &SerialPort::handleError);
    connect(this->serial_port, &QSerialPort::readyRead, this, &SerialPort::handleMessage);
    connect(&this->heartbeatTimer, &QTimer::timeout, this, &SerialPort::sendHeartBeat);
}

SerialPort::~SerialPort() {
    if (this->serial_port) {
        if (this->serial_port->isOpen())
            this->serial_port->close();
        delete this->serial_port;
    }
}

void SerialPort::send(QSharedPointer<MicroMessage> msg) {
    QByteArray buff = protocol.translate(msg);
    Logger::debug(SERIALPORT_SENDING_MSG, buff.toHex(SEPARATOR).
            toStdString().c_str(), msg->msgInfo().c_str());
    this->serial_port->write(buff);
}

void SerialPort::findDevice() {
    auto portsInfo = QSerialPortInfo::availablePorts();
    for(auto &info : portsInfo){
        if(info.serialNumber() == PORT_SERIAL_NUMBER){
            this->serial_port->setPort(info);
            this->serial_port->setBaudRate(BAUDRATE);
            this->serial_port->setDataBits(QSerialPort::Data8);
            this->serial_port->setParity(QSerialPort::NoParity);
            if(this->serial_port->open(QIODevice::ReadWrite)){
                reconectionTimer.stop();
                heartbeatTimer.start(HEARTBEAT_TIMEOUT);
                emit serialPortConnected();
                Logger::info(SERIALPORT_CONNECTED_MSG);
                return;
            } else {
                Logger::warning(SERIALPORT_CONNECT_FAILED_MSG);
            }
        }
    }
    Logger::warning(SERIAL_PORT_DEVICE_NOT_FOUND_MSG);
    reconectionTimer.start(RECONNECTION_TIMEOUT);
}

void SerialPort::resetState() {
    this->arrivingBytesCount = 0;
    this->heartbeatTimer.stop();
    this->heartbeatAcked = false;
    this->consecutiveErrors = 0;
    this->readingStatus = WAITING;
    this->arrivingPacket.clear();

}

void SerialPort::handleError(QSerialPort::SerialPortError error){
    if (error == 0)
        return;
    Logger::warning(SERIAL_PORT_HANDLE_ERROR_MSG,
        this->serial_port->errorString().toStdString().c_str(),
        (int) error);
    // Si el micro dio señales de vida o si no mandó ack pero no
    // transcurrieron N errores (pudo haber sido un error temporal
    // y que se arregló posteriormente) no cierro la conexión
    if (this->heartbeatAcked || this->consecutiveErrors < CONSECUTIVE_USB_ERROR_BEFORE_CLOSING_IT){
        // Tecnicamente hablando esto no es una RC ya que esta clase tiene scope unicamente en el hilo
        // principal. Hice atomica a la variable pero no era necesario.
        this->consecutiveErrors++;
        return;
    }
    if (this->serial_port->isOpen()) {
        this->serial_port->close();
    }
    resetState();
    emit serialPortDisconnected();
    reconectionTimer.start(RECONNECTION_TIMEOUT);

}

void SerialPort::handleMessage(){
    QByteArray readArray = this->serial_port->readAll();
    for(auto readByte : readArray){
        switch(readingStatus){
            case WAITING:
                if(readByte == 0x7E){
                    arrivingPacket[arrivingBytesCount] = readByte;
                    arrivingBytesCount++;
                    readingStatus = READING;
                }
                break;
            case READING:
                arrivingPacket[arrivingBytesCount] = readByte;
                arrivingBytesCount++;
                if (readByte == 0x7E) {
                    arrivingBytesCount = 0;
                    arrivingPacket[arrivingBytesCount] = readByte;
                    arrivingBytesCount++;
                } else if(arrivingBytesCount == PACKET_SIZE){
                    readingStatus = WAITING;
                    arrivingBytesCount = 0;
                    processMessage(arrivingPacket);
                }
                break;
        }
    }
}

void SerialPort::processMessage(QByteArray buff){
    uint8_t crc = crcChecksum(buff, PACKET_SIZE-1);
    if(crc==(uint8_t)buff[PACKET_SIZE-1]) {
        QSharedPointer<MicroMessage> msg(this->protocol.translate(buff));
        Logger::debug(SERIALPORT_RECEIVED_MSG, buff.toHex(SEPARATOR).
                toStdString().c_str(), msg->msgInfo().c_str());
        switch(msg->getId()) {
            case SHUTDOWN_ACKNOWLEDGE:
                Logger::info(SERIALPORT_SHUTDOWN_ACK_MSG);
                emit shutdownAcknowledge(QString::number(SHUTDOWN_ACKNOWLEDGE),
                        SERIALPORT_EMERGENCY_STOP_ACTIVATED_MSG);
                break;
            case TEMPERATURE_READING:
                emit temperatureArrived(msg);
                break;
            case COLD_JUNCTION_READING:
                emit coldJunctionArrived(msg);
                break;
            case THERMOCOUPLE_FAULT: {
                std::vector<QString> errors = ((ThermocoupleFault&)(*msg)).error();
                for (QString &msg: errors) {
                    emit thermocoupleFault(QString::number(THERMOCOUPLE_FAULT), msg);
                    Logger::info(SERIALPORT_THERMOCOUPLE_ERROR_RECEIVED_MSG, msg.toStdString().c_str());
                }
                break;
            } case THERMOCOUPLE_CONFIGURATION_ACKNOWLEDGE:
                emit configurationAcknowledge(QString::number(THERMOCOUPLE_CONFIGURATION_ACKNOWLEDGE),
                        SERIALPORT_THERMOCOUPLE_CONFIG_ACK + buff.toHex(SEPARATOR));
                Logger::info(SERIALPORT_THERMOCOUPLE_CONFIG_ACK);
                break;
            case POWER_SET_ACKNOWLEDGE:
                emit powerSetAcknowledge(msg);
                Logger::info(SERIALPORT_POWER_SET_ACK_MSG, ((PowerSetAcknowledge&)(*msg)).getPower());
                break;
            case MANUAL_CONTROL_ACKNOWLEDGE:
                emit manualControlAcknowledge();
                emit manualControlAcknowledge(QString::number(MANUAL_CONTROL_ACKNOWLEDGE),
                                              SERIALPORT_MANUAL_CONTROL_SET_ACK_MSG);
                Logger::info(SERIALPORT_MANUAL_CONTROL_SET_ACK_MSG);
                break;
            case AUTOMATIC_CONTROL_ACKNOWLEDGE:
                emit automaticControlAcknowledge();
                emit automaticControlAcknowledge(QString::number(AUTOMATIC_CONTROL_ACKNOWLEDGE),
                                                 SERIALPORT_AUTOMATIC_CONTROL_SET_ACK_MSG);
                Logger::info(SERIALPORT_AUTOMATIC_CONTROL_SET_ACK_MSG);
                break;
            case CURRENT_FREQUENCY_READING:
                emit currentFrequencyArrived(msg);
                break;
            case CURRENT_RMS_READING:
                emit currentRMSArrived(msg);
                break;
            case HEARTBEAT_ACK:
                this->heartbeatCame();
                break;
            default:
                Logger::warning(SERIALPORT_UNKOWN_MSG, buff.toHex(SEPARATOR).toStdString().c_str());
                break;
        }
    } else {
        Logger::warning(SERIALPORT_CRC_FAILED_MSG,
            buff.toHex(SEPARATOR).toStdString().c_str(), 
            (uint8_t) buff[7],
            crc);
    }
}

uint8_t SerialPort::crcChecksum(QByteArray data, uint8_t size){
    int16_t i;
    uint16_t chk = 0xFF;

    for (i=0; i < size; i++)
        chk -= data[i];

    chk &= 0xff;

    return (uint8_t)chk;
}

QString SerialPort::getPortName() {
    return this->serial_port->portName();
}

void SerialPort::sendHeartBeat() {
    this->heartbeatAcked = false;
    QSharedPointer<HeartBeat> msg(new HeartBeat());
    this->send(msg);

}
/**
 * Actualizo el booleano de que el heartbeat llegó, y reinicio a cero el contador
 */
void SerialPort::heartbeatCame() {
    this->heartbeatAcked = true;
    this->consecutiveErrors = 0;
}
