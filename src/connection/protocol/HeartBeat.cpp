/*
 * Created by fedemgp
 * Date: 17/4/21
 */

#include "HeartBeat.h"

HeartBeat::HeartBeat(): OutgoingMessage(HEARTBEAT) {}

std::string HeartBeat::msgInfo() {
    return "Heartbeat request";
}

QByteArray HeartBeat::serialize() {
    QByteArray ret(8, 0x00);
    ret[0] = MESSAGE_SEPARATOR;
    ret[1] = HEARTBEAT;
    return ret;
}
