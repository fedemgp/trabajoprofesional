/*
 * Created by fedemgp
 * Date: 17/4/21
 */

#include "HeartBeatAck.h"

std::string HeartBeatAck::msgInfo() {
    return "Heartbeat ACK";
}

HeartBeatAck::HeartBeatAck(): IncomingMessage(HEARTBEAT_ACK) {}
