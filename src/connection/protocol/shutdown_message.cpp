
/*
 * Created by Federico Manuel Gomez Peter
 * on 6/10/19.
 */

#include "shutdown_message.h"
#include "../../control/control_algorithm.h"

ShutdownMessage::ShutdownMessage(): OutgoingMessage(SHUTDOWN_MESSAGE) {}

QByteArray ShutdownMessage::serialize() {
    QByteArray ret(8, 0x00);
    ret[0] = MESSAGE_SEPARATOR;
    ret[1] = SET_POWER;
    ret[2] = ControlAlgorithm::powerToTaps(10);
    return ret;
}

std::string ShutdownMessage::msgInfo() {
    std::string ret = "shutdown";
    return ret;
}
