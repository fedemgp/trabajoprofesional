/*
 * Created by fedemgp
 * Date: 21/4/21
 */

#ifndef __UNKNOWN_MSG_H__
#define __UNKNOWN_MSG_H__


#include "incoming_message.h"

class UnknownMsg: public IncomingMessage {
public:
    explicit UnknownMsg();
    virtual ~UnknownMsg() = default;
    std::string msgInfo() override;
};


#endif // __UNKNOWN_MSG_H__