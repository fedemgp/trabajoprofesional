/*
 * Created by fedemgp
 * Date: 17/4/21
 */

#ifndef __HEARTBEATACK_H__
#define __HEARTBEATACK_H__


#include "incoming_message.h"

class HeartBeatAck: public IncomingMessage {
public:
    explicit HeartBeatAck();
    virtual ~HeartBeatAck() = default;
    std::string msgInfo() override;

};


#endif // __HEARTBEATACK_H__