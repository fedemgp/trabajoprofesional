/*
 * Created by Federico Manuel Gomez Peter
 * on 21/9/19.
 */

#include <iostream>
#include <logger/logger.h>
#include "messages.h"

#include "protocol.h"
#include "shutdown_ack.h"
#include "temperature_reading.h"
#include "cold_junction_reading.h"
#include "power_set_acknowledge.h"
#include "manual_control_acknowledge.h"
#include "automatic_control_acknowledge.h"
#include "thermocouple_fault.h"
#include "thermocouple_configuration_acknowledge.h"
#include "current_frequency_reading.h"
#include "current_RMS_reading.h"
#include "set_automatic_control.h"
#include "set_manual_control.h"
#include "set_power.h"
#include "shutdown_message.h"
#include "thermocouple_configuration.h"
#include "HeartBeatAck.h"
#include "unknown_msg.h"

Protocol::Protocol() {
    // Burocracia de QT para poder usar el event loop de QT y comunicar hilos mediantes signals y slots.
    Logger::debug("Registering types for qt queued connections");
    qRegisterMetaType<QSharedPointer<AutomaticControlAcknowledge>>("QSharedPointer<AutomaticControlAcknowledge>");
    qRegisterMetaType<QSharedPointer<ColdJunctionReading>>("QSharedPointer<ColdJunctionReading>");
    qRegisterMetaType<QSharedPointer<CurrentFrequencyReading>>("QSharedPointer<CurrentFrequencyReading>");
    qRegisterMetaType<QSharedPointer<CurrentRMSReading>>("QSharedPointer<CurrentRMSReading>");
    qRegisterMetaType<QSharedPointer<ManualControlAcknowledge>>("QSharedPointer<ManualControlAcknowledge>");
    qRegisterMetaType<QSharedPointer<PowerSetAcknowledge>>("QSharedPointer<PowerSetAcknowledge>");
    qRegisterMetaType<QSharedPointer<SetAutomaticControl>>("QSharedPointer<SetAutomaticControl>");
    qRegisterMetaType<QSharedPointer<SetManualControl>>("QSharedPointer<SetManualControl>");
    qRegisterMetaType<QSharedPointer<SetPower>>("QSharedPointer<SetPower>");
    qRegisterMetaType<QSharedPointer<ShutdownAcknowledge>>("QSharedPointer<ShutdownAcknowledge>");
    qRegisterMetaType<QSharedPointer<ShutdownMessage>>("QSharedPointer<ShutdownMessage>");
    qRegisterMetaType<QSharedPointer<TemperatureReading>>("QSharedPointer<TemperatureReading>");
    qRegisterMetaType<QSharedPointer<ThermocoupleConfigurationAcknowledge>>("QSharedPointer<ThermocoupleConfigurationAcknowledge>");
    qRegisterMetaType<QSharedPointer<ThermocoupleConfiguration>>("QSharedPointer<ThermocoupleConfiguration>");
    qRegisterMetaType<QSharedPointer<ThermocoupleFault>>("QSharedPointer<ThermocoupleFault>");

    qRegisterMetaType<QSharedPointer<IncomingMessage>>("QSharedPointer<IncomingMessage>");
    qRegisterMetaType<QSharedPointer<OutgoingMessage>>("QSharedPointer<OutgoingMessage>");
    qRegisterMetaType<QSharedPointer<MicroMessage>>("QSharedPointer<MicroMessage>");

}

QByteArray Protocol::translate(QSharedPointer<MicroMessage>msg) {
    return msg->serialize();
}

QSharedPointer<MicroMessage> Protocol::translate(QByteArray &buff) {
    uint8_t  id = buff[ID_POSITION];
    switch (id) {
        case SHUTDOWN_ACKNOWLEDGE:
            return QSharedPointer<MicroMessage>(new ShutdownAcknowledge());
        case TEMPERATURE_READING:
            return QSharedPointer<MicroMessage>(new TemperatureReading(buff));
        case COLD_JUNCTION_READING:
            return QSharedPointer<MicroMessage>(new ColdJunctionReading(buff));
        case THERMOCOUPLE_FAULT:
            return QSharedPointer<ThermocoupleFault>(new ThermocoupleFault(buff));
        case THERMOCOUPLE_CONFIGURATION_ACKNOWLEDGE:
            return QSharedPointer<MicroMessage>(new ThermocoupleConfigurationAcknowledge());
        case POWER_SET_ACKNOWLEDGE:
            return QSharedPointer<MicroMessage>(new PowerSetAcknowledge(buff));
        case MANUAL_CONTROL_ACKNOWLEDGE:
            return QSharedPointer<MicroMessage>(new ManualControlAcknowledge());
        case AUTOMATIC_CONTROL_ACKNOWLEDGE:
            return QSharedPointer<MicroMessage>(new AutomaticControlAcknowledge());
        case CURRENT_FREQUENCY_READING:
            return QSharedPointer<MicroMessage>(new CurrentFrequencyReading(buff));
        case CURRENT_RMS_READING:
            return QSharedPointer<MicroMessage>(new CurrentRMSReading(buff));
        case HEARTBEAT_ACK:
            return QSharedPointer<MicroMessage>(new HeartBeatAck());
        default:
            Logger::warning(SERIALPORT_INVALID_MSG_ERROR_MSG);
            break;
    }
    return QSharedPointer<MicroMessage>(new UnknownMsg());
}


