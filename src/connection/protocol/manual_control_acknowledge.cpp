
/*
 * Created by Federico Manuel Gomez Peter
 * on 13/10/19.
 */

#include "manual_control_acknowledge.h"

ManualControlAcknowledge::ManualControlAcknowledge():
            IncomingMessage(MANUAL_CONTROL_ACKNOWLEDGE) {}

std::string ManualControlAcknowledge::msgInfo() {
    std::string ret = "Manual control ACK";
    return ret;
}
