/*
 * Created by fedemgp
 * Date: 17/4/21
 */

#ifndef __HEARTBEAT_H__
#define __HEARTBEAT_H__


#include "outgoing_message.h"

class HeartBeat: public OutgoingMessage {
public:
    HeartBeat();
    virtual ~HeartBeat() = default;
    std::string msgInfo() override;
    QByteArray serialize() override;
};


#endif // __HEARTBEAT_H__