/*
 * Created by fedemgp
 * Date: 21/4/21
 */

#include "unknown_msg.h"

UnknownMsg::UnknownMsg(): IncomingMessage(UNKNOWN_ERROR) {}

std::string UnknownMsg::msgInfo() {
    return "Mensaje inválido";
}
