//
// Created by Martín García on 18/09/2019.
//

#ifndef TRABAJOPROFESIONAL_SERIALPORT_H
#define TRABAJOPROFESIONAL_SERIALPORT_H

#include <QSerialPort>
#include <QSerialPortInfo>
#include <QTimer>
#include <QMutex>
#include <QSharedPointer>
#include "protocol/protocol.h"

typedef enum {WAITING, READING} reading_status_t;

class SerialPort: public QObject{
    Q_OBJECT
private:
    QSerialPort *serial_port = nullptr;
    Protocol protocol;
    QTimer reconectionTimer;
    /**
     * Se controlará el estado de la conexión usb mediante un "heartbeat".
     * Cada N segundos se enviará un mensaje al micro para notificarle
     * que la conexión está bien. Si el micro no recibe el mensaje, dejará de enviar datos.
     *
     * Si la app tiene fallos consecutivos sin que le haya llegado un ACK del heartbeat,
     * cerrará la conexión y volverá a intentar conectarse
     */
    QTimer heartbeatTimer;
    std::atomic<uint32_t> consecutiveErrors;
    std::atomic<bool> heartbeatAcked;
    int arrivingBytesCount = 0;
    reading_status_t readingStatus = WAITING;
    QByteArray arrivingPacket;
    void processMessage(QByteArray);
    uint8_t crcChecksum(QByteArray, uint8_t);
    void resetState();
    SerialPort(SerialPort &copy) = delete;
    SerialPort &operator=(const SerialPort &copy) = delete;
    SerialPort(SerialPort &&other) = delete;
    SerialPort &operator=(SerialPort &&other) = delete;
    void heartbeatCame();
public:
    explicit SerialPort(QObject *parent);
    ~SerialPort() override;

    void send(QSharedPointer<MicroMessage> msg);
    QString getPortName();

public slots:
    void findDevice();
    void handleError(QSerialPort::SerialPortError);
    void handleMessage();
    void sendHeartBeat();

signals:
    void shutdownAcknowledge(QString code, QString desc);
    void configurationAcknowledge(QString code, QString desc);
    void thermocoupleFault(QString code, QString desc);
    void temperatureArrived(QSharedPointer<MicroMessage> msg);
    void coldJunctionArrived(QSharedPointer<MicroMessage> msg);
    void powerSetAcknowledge(QSharedPointer<MicroMessage> msg);
    void manualControlAcknowledge();
    void manualControlAcknowledge(QString code, QString desc);
    void automaticControlAcknowledge();
    void automaticControlAcknowledge(QString code, QString desc);
    void serialPortConnected();
    void serialPortDisconnected();
    void currentFrequencyArrived(QSharedPointer<MicroMessage> msg);
    void currentRMSArrived(QSharedPointer<MicroMessage> msg);
};


#endif //TRABAJOPROFESIONAL_SERIALPORT_H
