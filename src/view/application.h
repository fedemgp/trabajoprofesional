#ifndef __APPLICATION_H__
#define __APPLICATION_H__

#include <QApplication>

class Application : public QApplication {
public:
    Application(int& argc, char** argv);
    bool notify(QObject* receiver, QEvent* event); 
};

#endif