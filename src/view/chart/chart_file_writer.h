//
// Created by fedemgp on 30/10/20.
//

#ifndef TRABAJOPROFESIONAL_CHART_FILE_WRITER_H
#define TRABAJOPROFESIONAL_CHART_FILE_WRITER_H

#include <QtCore/QThread>
#include <QString>

#include <vector>
#include <fstream>
#include <stream.h>
#include <string>

enum class PointType {EXIT, POWER, FRECUENCY, CURRENT, TEMPERATURE};

struct ChartPoint {
    double time;
    double value1;
    double value2;
    PointType type;
    ChartPoint(): time(0), value1(0), value2(0), type(PointType::EXIT){};
    ChartPoint(double time, double value1, double value2, PointType type): time(time), value1(value1), value2(value2), type(type) {};
};
class ChartFileWriter: public QThread {
    Q_OBJECT
public:
    ChartFileWriter(QString &directory);
    ~ChartFileWriter();
    void pushPoint(ChartPoint point);
    void stop();
private:
    void run() override;
    void openFile(QString dir, std::ofstream &file, const char *seriesName1, const char *seriesName2);
    /*
     * Archivos donde se guaradarán los distintos datos reicbidos
     */
    std::ofstream current_frecuency_file;
    std::ofstream temperature_power_file;
    std::atomic<bool> keep_processing{true};
    IO::Stream<ChartPoint> queue;

    void writeData(ChartPoint &point, std::ofstream &fstream);
};


#endif //TRABAJOPROFESIONAL_CHART_FILE_WRITER_H
