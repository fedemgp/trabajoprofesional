//
// Created by fedemgp on 30/10/20.
//

#include <messages.h>
#include <logger/logger.h>
#include <QtCore/QDateTime>
#include <QDir>
#include "chart_file_writer.h"

ChartFileWriter::~ChartFileWriter() {
    this->stop();
    this->queue.close();
}

ChartFileWriter::ChartFileWriter(QString &directory) {
    //openFile(directory + "/corriente", this->current_file, "Corriente[A]");
    //openFile(directory + "/frecuencia", this->frecuency_file, "Frecuencia[Hz]");
    directory += QDir::separator();

    openFile(directory + "temperatura-potencia", this->temperature_power_file, "Temperatura[°C]", "Potencia[%]");
    QThread::start();
}

void ChartFileWriter::openFile(QString filename, std::ofstream &stream, const char *seriesName1, const char *seriesName2) {
    QDateTime currentTime = QDateTime::currentDateTime();
    filename += "-" + currentTime.toString("yyyy-MM-dd-hhmmss") + ".csv";
    std::string file = filename.toStdString();
    Logger::info(FILE_SAVED_MSG, file.c_str());
    stream.open(file, std::ios_base::out);
    // Escribo el header
    stream << "Tiempo[hh:mm:ss.zzz]," << seriesName1 << "," << seriesName2 << std::endl;
}

void ChartFileWriter::run() {
    try {
        while(keep_processing) {
            ChartPoint point;
            this->queue.pop(point);

            switch (point.type) {
                case PointType::CURRENT: {
                    //writeData(point, temperature_power_file);
                    break;
                }
                case PointType::FRECUENCY: {
                    //writeData(point, frecuency_file);
                    break;
                }
                case PointType::POWER: {
                    writeData(point, temperature_power_file);
                    break;
                }
                case PointType::TEMPERATURE: {
                    writeData(point, temperature_power_file);
                    break;
                }
                case PointType::EXIT: {
                    return;
                }
            }
        }
    } catch (std::exception &e) {
        Logger::critical(e.what());
    } catch (...) {
        Logger::critical(UNKNOWN_ERROR_MSG, "main");
    }
}

void ChartFileWriter::writeData(ChartPoint &point, std::ofstream &file) {
    QDateTime x = QDateTime::fromMSecsSinceEpoch(point.time);
    file << x.toString("hh:mm:ss.zzz").toStdString() << "," << point.value1 << "," << point.value2 << std::endl;
}

void ChartFileWriter::stop() {
    this->pushPoint(ChartPoint(0, 0, 0, PointType::EXIT));
    this->keep_processing = false;
}

void ChartFileWriter::pushPoint(ChartPoint point) {
    this->queue.push(point);
}
