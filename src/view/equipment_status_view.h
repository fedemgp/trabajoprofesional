#ifndef EQUIPMENT_STATUS_VIEW_H
#define EQUIPMENT_STATUS_VIEW_H

#include <QWidget>
#include <QTimer>
#include "../connection/serialport.h"

namespace Ui {
class EquipmentStatusView;
}

class EquipmentStatusView : public QWidget
{
    Q_OBJECT

public:
    explicit EquipmentStatusView(QWidget *parent, SerialPort *pPort);
    ~EquipmentStatusView();
    void thermocoupleFault(QString &code, QString &description);

public slots:
    void insert(QString, QString);
    void onSerialPortConnected();
    void onSerialPortDisconnected();
    void resetThermocoupleLabel();

private:
    Ui::EquipmentStatusView *ui;
    SerialPort *port;
    QTimer thermocoupleLabelTimer;
};

#endif // EQUIPMENT_STATUS_VIEW_H
