#ifndef LEGACY_AUTOTUNNING_TAB_VIEW_H
#define LEGACY_AUTOTUNNING_TAB_VIEW_H

#include <QWidget>
#include <connection/protocol/temperature_reading.h>
#include <connection/serialport.h>
#include <control/legacy_ziegler_nichols.h>
#include <QIntValidator>
#include <QDoubleValidator>

namespace Ui {
class legacy_autotunning_tab_view;
}

class MainWindow;

class LegacyAutotunningTabView : public QWidget
{
    Q_OBJECT

public:
    explicit LegacyAutotunningTabView(QWidget *parent, SerialPort *port);
    ~LegacyAutotunningTabView();
    void enableButtons(bool enable);
    void stop(bool finished, bool printError);
    void dataAvailable(TemperatureReading &temp);

signals:
    void ZNCalculated(float kp, float ki, float kd);
    void printMessage(const char *str, unsigned char mode, bool reset);

public slots:
    void on_deactivateButton_clicked(bool finished = false);
private slots:
    void on_activateButton_clicked();
    void znCalculated(float kp, float ki, float kd);
    void znFailed();
    void autotunningFailed(const char *reason);
    void sendPower(QSharedPointer<MicroMessage> msg);

private:
    bool validateInput();
    Ui::legacy_autotunning_tab_view *ui;
    MainWindow* mainWindow;
    SerialPort *port;
    std::unique_ptr<LegacyZieglerNichols> zn;
    QIntValidator *powerValidator;
    QDoubleValidator *tempValidator;
    QDoubleValidator *tempSensitivityValidator;
};

#endif // LEGACY_AUTOTUNNING_TAB_VIEW_H
