#include <messages.h>
#include <connection/protocol/shutdown_message.h>
#include "legacy_autotunning_tab_view.h"
#include "ui_legacy_autotunning_tab_view.h"
#include "general_view.h"

LegacyAutotunningTabView::LegacyAutotunningTabView(QWidget *parent, SerialPort *port) :
    QWidget(parent),
    ui(new Ui::legacy_autotunning_tab_view),
    mainWindow((MainWindow *)parent),
    port(port) {
    ui->setupUi(this);
    this->powerValidator = new QIntValidator(0, 100);
    this->tempValidator = new QDoubleValidator(-99999, 99999, 2);
    this->tempSensitivityValidator = new QDoubleValidator(0, 100, 3);
    this->ui->sensitivityEdit->setValidator(this->tempSensitivityValidator);
    this->ui->cutoffTempEdit->setValidator(this->tempValidator);
    this->ui->minimumPowerEdit->setValidator(this->powerValidator);
}

LegacyAutotunningTabView::~LegacyAutotunningTabView()
{
    delete this->powerValidator;
    delete this->tempValidator;
    delete this->tempSensitivityValidator;
    delete ui;
}

void LegacyAutotunningTabView::on_activateButton_clicked() {
    if(!mainWindow->isControlActivated() && zn == nullptr) {
        if (!validateInput()) {
            emit printMessage(AUTOTUNNING_VIEW_BAD_ARGUMENTS, ERROR, true);
            Logger::info(AUTOTUNNING_VIEW_BAD_ARGUMENTS);
            return;
        }
        emit printMessage(AUTOTUNING_VIEW_PROCESSING_MSG, 2, false);
        Logger::info(ZN_ACTIVATED_MSG);
        int initial_power = this->ui->minimumPowerEdit->text().toInt();
        // De acuerdo a lo que hace el codigo de alberto
        int stationary_power = initial_power + 25;
        float cutoff_temp = this->ui->cutoffTempEdit->text().toFloat();
        float temp_sensitivity = this->ui->sensitivityEdit->text().toFloat();
        // me desconecto de todas las señales que este objeto lance
        if (zn != nullptr) {
            disconnect(zn.get(), nullptr, this, nullptr);
        }
        zn.reset(new LegacyZieglerNichols(initial_power, stationary_power, cutoff_temp, temp_sensitivity));
        connect(zn.get(), &LegacyZieglerNichols::znCalculated, this, &LegacyAutotunningTabView::znCalculated);
        connect(zn.get(), &ControlAlgorithm::setPower, this, &LegacyAutotunningTabView::sendPower);
        connect(zn.get(), &LegacyZieglerNichols::znFailed, this, &LegacyAutotunningTabView::znFailed);
        zn->start();
    } else{
        emit printMessage(ZN_CANT_BE_ACTIVATED_MSG, ERROR, true);
        Logger::info(ZN_CANT_BE_ACTIVATED_MSG);
    }
}

void LegacyAutotunningTabView::enableButtons(bool enable) {
    this->ui->activateButton->setEnabled(enable);
    this->ui->deactivateButton->setEnabled(enable);
}


void LegacyAutotunningTabView::znFailed() {
    // me desconecto de todas las señales que este objeto lance
    if (zn != nullptr) {
        disconnect(zn.get(), nullptr, this, nullptr);
    }
    zn.reset(nullptr);
    QSharedPointer<MicroMessage> msg(new ShutdownMessage());
    port->send(msg);
    emit printMessage(ZN_FAILED, ERROR, true);
    Logger::warning(ZN_FAILED);
}

void LegacyAutotunningTabView::stop(bool finished, bool printError) {
    if (zn == nullptr) {
        if (printError)
                emit printMessage(AUTOTUNNING_NO_PROCEES_TO_DEACTIVATE_MSG, ERROR, true);
        return;
    }
    // me desconecto de todas las señales que este objeto lance
    if (zn != nullptr) {
        disconnect(zn.get(), nullptr, this, nullptr);
    }
    zn.reset(nullptr);
    if (finished) {
        emit printMessage(ZN_SUCCESFULY_FINISHED, OK, true);
        Logger::info(ZN_SUCCESFULY_FINISHED);
    } else {
        emit printMessage(ZN_INTERRUPTED, ERROR, true);
        Logger::warning(ZN_INTERRUPTED);
    }
}

void LegacyAutotunningTabView::dataAvailable(TemperatureReading &temp) {
    if (zn != nullptr) {
        zn->receiveData(temp);
    }
}

void LegacyAutotunningTabView::on_deactivateButton_clicked(bool finished) {
    this->stop(finished, true);
    QSharedPointer<MicroMessage> msg(new ShutdownMessage());
    port->send(msg);
}

void LegacyAutotunningTabView::znCalculated(float kp, float ki, float kd) {
    emit ZNCalculated(kp, ki, kd);
}

void LegacyAutotunningTabView::autotunningFailed(const char *reason) {
    emit printMessage(reason, ERROR, true);
}

void LegacyAutotunningTabView::sendPower(QSharedPointer<MicroMessage> msg) {
    this->port->send(msg);
}

bool LegacyAutotunningTabView::validateInput() {
    QString initial_power = this->ui->minimumPowerEdit->text();
    QString cutoff_temp = this->ui->cutoffTempEdit->text();
    QString temp_sensitivity = this->ui->sensitivityEdit->text();

    if (initial_power == "" || cutoff_temp == "" || temp_sensitivity == "")
        return false;

    int d = 0;
    auto ipower_state = this->powerValidator->validate(initial_power, d);
    auto ctemp_state = this->tempValidator->validate(cutoff_temp, d);
    auto temps_state = this->tempSensitivityValidator->validate(temp_sensitivity, d);

    if (ipower_state != QValidator::Acceptable ||
        ctemp_state != QValidator::Acceptable ||
        temps_state != QValidator::Acceptable)
        return false;

    return true;
}
