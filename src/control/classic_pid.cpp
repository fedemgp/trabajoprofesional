//
// Created by Martín García on 11/11/2019.
//

#include <sstream>
#include "messages.h"
#include <numeric>
#include <cmath>

#include "classic_pid.h"
#include "logger/logger.h"
#include "../configuration/app_config.h"


ClassicPID::ClassicPID(float kp, float ki, float kd, float targetTemp, float deadzone, uint8_t window_size):
        ControlAlgorithm(targetTemp, deadzone, window_size), integralErrorLimit(0),
        Kp(kp), Ki(ki), Kd(kd) {
	Logger::debug(CLASSIC_PID_CONSTRUCTOR_MSG, kp, kd, ki, targetTemp);
	ApplicationConfig &appConfig = ApplicationConfig::instance();
    integralActionLimit = appConfig.getIntegralActionLimit();
	if (ki != 0)
	    integralErrorLimit = std::abs(appConfig.getAntiwindup() / ki);
}

unsigned char ClassicPID::process(QSharedPointer<TemperatureReading> temp) {
	/**
	 *	El control se va a aplicar usando una ventana de tiempo, donde se 
	 *	calcula el error medio de esa ventana.
	 *
	 *	A nivel código, esto significa que el programa agregará elementos al
	 *	vector hasta que el tamaño de este sea el de la ventana. Cuando esto
	 *	suceda, cada temperatura nueva reemplazará a la temperatura mas antigua,
	 *	en formato round-robin.
	 */
  std::lock_guard<std::mutex> lock(this->m);
	if (errorValues.size() < this->window_size)
		errorValues.emplace_back(this->targetTemp - temp->getData());
	else
		this->errorValues[iteration % this->window_size] = this->targetTemp - temp->getData();
	iteration++;

	errorMean = std::accumulate(this->errorValues.begin(),
								this->errorValues.end(), 0.0f);
	errorMean /= this->errorValues.size();

	derivativeError = errorMean - previousErrorMean; 
	previousErrorMean = errorMean;
	// Comienzo a tener acción integral 100 grados antes de llegar a la temp obj
	if (std::abs(errorMean) < integralActionLimit)
		integralError += errorMean;
	if (std::abs(integralError) >= integralErrorLimit)
		if (integralError > 0)
	    	integralError = integralErrorLimit;
	    else
	    	integralError = -integralErrorLimit;
	/**
	 *	Segun la implementación vieja, el control PID devuelve las vueltas
	 *	que se le debe aplicar al potenciometro (debería ser potencia). 
	 */
	float power = (Kp * errorMean + Kd * derivativeError + Ki * integralError);
	return ControlAlgorithm::powerToTaps(power);
}
