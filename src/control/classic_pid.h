//
// Created by Martín García on 11/11/2019.
//

#ifndef TRABAJOPROFESIONAL_CLASSIC_PID_H
#define TRABAJOPROFESIONAL_CLASSIC_PID_H

#include <QSharedPointer>
#include "control_algorithm.h"
#include <cstdint>
#include <mutex>

class ClassicPID : public ControlAlgorithm {
protected:
    float integralErrorLimit;
    float Kp = 0, Ki = 0, Kd = 0;
    float errorMean = 0;
    float previousErrorMean = 0;
    float derivativeError = 0;
    float integralError = 0;
    float integralActionLimit = 0;

public:
	ClassicPID(float kp, float ki, float kd, float targetTemp, float deadzone, uint8_t window_size);
	virtual ~ClassicPID() = default;
	/**
	 *	Dado una temperatura nueva, calcula por medio del control clásico
	 *	la potencia que debe aplicar el horno. El valor de retorno es la
	 *	cantidad de vueltas que debe dar el potenciómetro electrónico.
	 */
	virtual unsigned char process(QSharedPointer<TemperatureReading> data) override;
};


#endif //TRABAJOPROFESIONAL_CLASSIC_PID_H
