/*
 * Created by fedemgp
 * Date: 26/4/21
 */

#include <connection/protocol/set_power.h>
#include <logger/logger.h>
#include <configuration/app_config.h>
#include "legacy_ziegler_nichols.h"
#include "zn_constants.h"

#define DIFF_WINDOW_SIZE 3

LegacyZieglerNichols::LegacyZieglerNichols(int initial_power, int stationary_power, float cutoff_temp,
                                           float temp_sensitivity):
                                                ControlAlgorithm(0, 0.0f, ApplicationConfig::instance().getWindowSize()),
                                                min_power(initial_power),
                                                max_power(stationary_power),
                                                cutoff_temp(cutoff_temp),
                                                temp_sensitivity(temp_sensitivity) {
    QSharedPointer<MicroMessage> msg(new SetPower(powerToTaps(100)));
    emit setPower(msg);
    this->state = STEP_1;
}

LegacyZieglerNichols::~LegacyZieglerNichols() {}

unsigned char LegacyZieglerNichols::process(QSharedPointer<TemperatureReading> data) {
    switch (this->state) {
        case STEP_1:
            if (data->getData() > this->cutoff_temp) {
                this->nextState();
                return powerToTaps(this->min_power);
            }
            return powerToTaps(100);
        case STEP_2:
            insertToCircularBuffer(tempBuffer, data->getData(), ZN_WINDOW_SIZE, tempBufferCounter);
            if (this->isTemperatureStable()) {
                this->nextState();
                return powerToTaps(this->max_power);
            }
            return powerToTaps(this->min_power);
        case STEP_3: {
            // Emulo la versión antigua del codigo de alberto. En este se hacia un promediado por hardware para
            // suavizar la curva de temperatura. Una vez que junto N muestras empiezo a tomar promedios e insertarlos
            // en un segundo buffer.
            insertToCircularBuffer(tempMeanBuffer, data->getData(), this->window_size, tempMeanBufferCounter);
            if (tempMeanBuffer.size() == this->window_size) {
                tempBuffer.emplace_back(getMean(tempMeanBuffer, this->window_size));
                if (tempBuffer.size() == DIFF_WINDOW_SIZE) {
                    // con las 3 muestras, hago la diferencia entre los pares (estimo la derivada) y calculo la media de
                    // las dos derivadas calculadas
                    // TODO: Respeté lo que hizo alberto para que le quede lo mismo a nicolás, pero
                    //       hay métodos numéricos mas precisos
                    float diff_mean = (tempBuffer[2] - tempBuffer[0]) / 2.0f;
                    this->derivativeTempBuffer.emplace_back(diff_mean);
                    this->tempBuffer.clear();

                    if (this->inflectionPointReached()) {
                        this->nextState();
                        return powerToTaps(10);
                    }
                }
            }
            return powerToTaps(this->max_power);
        }
        case FINISHED:
            throw Exception("Proceso terminado, no se pueden procesar mas eventos");
    }
    return 0;
}

bool LegacyZieglerNichols::isTemperatureStable() {
    float avg = std::accumulate(tempBuffer.begin(), tempBuffer.end(), 0);
    avg /= ZN_WINDOW_SIZE;
    for(auto temp : tempBuffer){
        if(temp - avg > temp_sensitivity)
            return false;
    }
    return true;
}

bool LegacyZieglerNichols::inflectionPointReached() {
    auto &buffer = this->derivativeTempBuffer;
    float max_derivative = *std::max_element(this->derivativeTempBuffer.begin(), this->derivativeTempBuffer.end());
    float last_derivative = this->derivativeTempBuffer[this->derivativeTempBuffer.size() - 1];
    float second_last_derivative = 0;
    // pongo un margen de maniobra para que no sea tan extricta la condición de corte, ante una minima variación ya
    // hay una condición de corte
    if (this->derivativeTempBuffer.size() > 2)
        second_last_derivative = this->derivativeTempBuffer[this->derivativeTempBuffer.size() - 2];
    else
        second_last_derivative = last_derivative;
    // La condicion de corte ahora es que las ultimas dos derivadas estimadas sean menor que la maxima
    if (max_derivative != last_derivative && max_derivative != second_last_derivative) {
        return true;
    }
    return false;
}


void LegacyZieglerNichols::nextState() {
    switch (this->state) {
        case STEP_1:
            this->state = STEP_2;
            break;
        case STEP_2:
            this->state = STEP_3;
            this->tempBuffer.clear();
            this->tempBufferCounter = 0;
            break;
        case STEP_3:
            this->keep_processing = false;
            this->state = FINISHED;
            if (this->calculateParameters())
                emit znCalculated(this->kp, this->ki, this->kd);
            else
                emit znFailed();
            break;
        case FINISHED:
            throw Exception("Proceso terminado, no se pueden procesar mas eventos");
    }
}

bool LegacyZieglerNichols::calculateParameters() {
    // la condición de corte de la etapa 3 es que la ultima pendiente no sea la maxima, por lo tanto la anteultima
    // pendiente es por definición la máxima
    uint32_t max_index = this->derivativeTempBuffer.size() - 1;
    // el tau es cuantas muestras tardó la app en encontrar el punto de inflexión
    float tau = max_index * DIFF_WINDOW_SIZE;
    float alfa = this->derivativeTempBuffer[max_index - 1];
    this->kp = (0.9f*(this->max_power - this->min_power))/(alfa*tau);
    // Esto no debería pasar nunca, ya que por lo menos debe haber 2 elementos en el vector de pendientes por
    // definición (el primer elemento que tiene una pendiente mayor a la ultima, sino no estaría acá).
    // Alberto lo tiene, asique lo dejo, pero si pasa va a pinchar en la asignación de alfa.
    if (tau == 0)
        this->ki = 0;
    else
        this->ki = this->kp / (3 * tau);
    // este método siempre considera que el control derivativo debe ser 0
    this->kd = 0;
    Logger::debug("kp: %.2f   ki: %.2f   kd: %.2f", kp, ki, kd);
    return this->ki >= 0 && this->kp > 0;
}

void LegacyZieglerNichols::insertToCircularBuffer(std::vector<float> &buffer, float temp, uint32_t window_size, uint64_t &idx) {
    if(buffer.size() < window_size)
        buffer.emplace_back(temp);
    else
        buffer[idx % window_size] = temp;
    idx++;
}

float LegacyZieglerNichols::getMean(std::vector<float> vector, uint8_t size) {
    float retValue = std::accumulate(vector.begin(), vector.end(), 0.0f);
    retValue /= (float) size;
    return retValue;
}

// Overrideo el método, no quiero que se actualice el valor de window_size cuando el proceso está activo
void LegacyZieglerNichols::updateConfig() {}
