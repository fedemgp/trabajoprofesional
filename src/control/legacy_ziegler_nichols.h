/*
 * Created by fedemgp
 * Date: 26/4/21
 */

#ifndef __LEGACY_ZIEGLER_NICHOLS_H__
#define __LEGACY_ZIEGLER_NICHOLS_H__


#include "control_algorithm.h"
typedef enum {STEP_1, STEP_2, STEP_3, FINISHED} legacy_zn_state_t;
/**
 * clase que reescribe el modelo legacy de ziegler nichols (hecho por Alberto Stolowitch).
 * Este proceso calcula los parámetros de un control PI en base a:
 *
 * * calentar el DUT a 100% hasta que se alcance la temperatura de corte que ingresa el usuario.
 * * bajar la potencia a 50%, y esperar hasta que la temperatura esté estable.
 * * Subir la potencia a 75% y tomar las muestras
 *
 * El algoritmo, una vez que está tomando muestras en la tercera etapa, empieza a calcular la derivada discreta
 * tomando de a 3 elementos y promediando. Sigue procesando hasta que se alcance el punto de inflexión de la curva.
 * Esto sucede cuando la recta pendiente deja de crecer (el ultimo elemento del vector no es el de pendiente mayor).
 * Esta seŕa la condición de corte.
 *
 * A partir de ahí, calcula los parámetros de unas ecuaciones que desconozco, y obtiene un PI.
 *
 */
class LegacyZieglerNichols : public ControlAlgorithm{
Q_OBJECT
public:
    LegacyZieglerNichols(int initial_power, int stationary_power, float cutoff_temp,
                         float temp_sensitivity);
    virtual ~LegacyZieglerNichols();
    unsigned char process(QSharedPointer<TemperatureReading> data) override;

protected:
    legacy_zn_state_t state = STEP_1;
    /**
     * Toma las ultimas ZN_WINDOW_SIZE muestras de temperatura, las promedia y
     * chequea que el error absoluto de cada una de esas muestras sea menor
     * que <temp_sensitivity> °c. Si lo es, devuelve true, sino devuelve false.
     */
    bool isTemperatureStable();
    /**
     * Método empleado por Alberto para encontrar la condición de corte de la etapa 3. Se asume que la curva de
     * respuesta al escalón es monotonamente creciente durante la primera fase de la curva en forma de S (por tema de
     * ruidos en el horno esto puede no es verdad). En dicho caso, el punto de inflexión
     * se va dar cuando la derivada máxima NO se encuentre en el último elemento del vector de derivadas (fue
     * creciendo hasta que se alcanza el punto de inflexión, en donde la pendiente de la curva empieza a disminuir).
     *
     * Esta función recorrerá el vector de pendientes de la curva, obteniendo el maximo y chequeando que la pendiente
     * maxima no sea la ultima pendiente leida.
     */
    bool inflectionPointReached();
    void nextState();
    bool calculateParameters();

signals:
    void znCalculated(float kp, float ki, float kd);
    void znFailed();

protected:
    float kp = 0, kd = 0, ki = 0;
private:
    int min_power;
    int max_power;
    float cutoff_temp;
    float temp_sensitivity;
    // Vector de N elementos (N siendo la configuración del <window_size> de ControlAlgorithm)
    std::vector<float> tempMeanBuffer;
    uint64_t tempMeanBufferCounter = 0;
    // Vector de maximo 3 elementos, se usará para respetar el algoritmo original. Se toman de a tres muestras,
    // se obtienen de estas tres muestras una estimación de la derivada y se guarda en derivativeTempBuffer
    std::vector<float> tempBuffer;
    uint64_t tempBufferCounter = 0;
    std::vector<float> derivativeTempBuffer;
    // TODO: hacer una clase circularBuffer que encapsule esto.
    void insertToCircularBuffer(std::vector<float> &buffer, float temp, uint32_t windows_size, uint64_t &idx);
    float getMean(std::vector<float> vector, uint8_t size);
    void updateConfig() override;
};


#endif // __LEGACY_ZIEGLER_NICHOLS_H__