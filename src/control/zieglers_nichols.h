//
// Created by Martín García on 15/02/2020.
//

#ifndef TRABAJOPROFESIONAL_ZIEGLERS_NICHOLS_H
#define TRABAJOPROFESIONAL_ZIEGLERS_NICHOLS_H


#include "control_algorithm.h"
#include <QSharedPointer>

typedef enum {POWER_AT_10, POWER_AT_20, UNDEFINED} power_level_t;


class ZieglerNichols : public ControlAlgorithm {
Q_OBJECT
public:
    explicit ZieglerNichols(int initial_power,
                            int stationary_power, float cutoff_temp,
                            float temp_sensitivity);
    unsigned char process(QSharedPointer<TemperatureReading> data) override;

protected:
    power_level_t powerLevel = UNDEFINED;
    /**
     * Toma las ultimas ZN_WINDOW_SIZE muestras de temperatura, las promedia y
     * chequea que el error absoluto de cada una de esas muestras sea menor
     * que <temp_sensitivity> °c. Si lo es, devuelve true, sino devuelve false.
     */
    bool isTemperatureStable();
    void nextState();
    void calculateParameters(float &kp, float &kd, float &ki);

signals:
    void errorOcurred(const char *msg);
    void znCalculated(float kp, float ki, float kd);


private:
    int min_power;
    int max_power;
    float cutoff_temp;
    float temp_sensitivity;
    std::vector<float> tempBuffer;
    std::vector<QSharedPointer<TemperatureReading>> stepResponse;
    uint64_t buffCounter = 0;
};


#endif //TRABAJOPROFESIONAL_ZIEGLERS_NICHOLS_H
