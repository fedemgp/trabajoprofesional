
/*
 * Created by Federico Manuel Gomez Peter
 * on 20/5/20.
 */

#include <exception.h>
#include <sstream>
#include "messages.h"

#include "logger/logger.h"
#include "MemberFunction.h"

MemberFunction::MemberFunction(float a, float b, float c, float d, std::string tag):
    a(a), b(b), c(c), d(d), tag(tag) {
    if (a > b || b > c || c > d) {
        throw Exception(MEMBER_FUNCTIONS_BAD_FORMAT_MSG, a, b, c, d);
    }
    // TODO: esta formual vale porque definimos las member functions simétricas.
    this->center = (d + a) / 2;

}

MemberFunction::MemberFunction(float a, float b, float c, float d, bool first, bool last, std::string tag):
        a(a), b(b), c(c), d(d), tag(tag) {
    if (a > b || b > c || c > d) {
        throw Exception(MEMBER_FUNCTIONS_BAD_FORMAT_MSG, a, b, c, d);
    }
    // TODO: esta formual vale porque definimos las member functions simétricas.
    this->center = (d + a) / 2;
    this->first = first;
    this->last = last;
}

float MemberFunction::calculate(float x) {
    if (first && x <= (b+c)/2) {
        return 1;
    }

    if (last && x >= (b+c)/2) {
        return 1;
    }

    if (x >= a && x <= b) {
        if (b == a)
            return 1;
        else
            return (x - a) / (b - a);
    }

    else if (x >= b && x <= c) {
        return 1;
    }

    else if (x >= c && x <= d) {
        if (d == c)
            return 1;
        else
            return (d - x) / (d - c);
    }
    else
        return 0;
}

const std::string &MemberFunction::getTag() const {
    return this->tag;
}

float MemberFunction::getCenter() const {
    return this->center;
}

void MemberFunction::print() const {
    std::ostringstream oss;
    oss << this->tag << ": [" << a << ", " << b << ", " << c << ", " << d 
        << "]";
    Logger::debug(oss.str().c_str());
}

void MemberFunction::setFirst() {
    this->first = true;
}

void MemberFunction::setLast() {
    this->last = true;
}

