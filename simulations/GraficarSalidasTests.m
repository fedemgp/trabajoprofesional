clear all;
close all;
clc;
csv_directory = "../build/"
dut = {"SecondOrderDut", "FirstOrderDut"}
dut_translate = {" segundo orden", " primer orden"}


csv_data = dlmread(strcat(csv_directory, dut{1}, "EndsWithMaxDegressIfWeAlwaysSetTo100PercentPower.csv"), ",", 1, 0);
figure
plot(csv_data(:, 2), '.');
title(strcat("Respuesta al escalon del DUT de ", dut_translate{1}))
grid minor
ylabel("Temperatura (°C)")
print("-S1200,800", "open_loop_second_order_normal.png");

csv_data = dlmread(strcat(csv_directory, dut{1}, "ZieglerNichols.csv"), ",", 1, 0);
figure
P1 = subplot(2,1,1)
x = 1:length(csv_data(:,1));
plot(x, csv_data(:,1))
title(strcat("Calculo de Ziegler Nichols para el dut de", dut_translate{1}))
ylabel("Potencia (%)")
axis([0, length(csv_data(:,1)), 0, 110])
grid minor
P2 = subplot(2,1,2)
plot(x, csv_data(:,2))
ylabel("Temperatura (°C)")
axis([0, length(csv_data(:,1)), 0, 2000])
grid minor
print("-S1200,800", "zn_second_order_normal.png");
set(P1, 'YLim', [0, 110]);
set(P1, 'XLim', [2790, 2817]);
set(P2, 'YLim', [320, 360]);
set(P2, 'XLim', [2790, 2817]);
print("-S1200,800", "zn_second_order_zoom.png");

csv_data = dlmread(strcat(csv_directory, dut{1}, "WillStabilizeAt900DegreesWithPIDControl.csv"), ",", 1, 0);
figure
P1 = subplot(2,1,1)
x = 1:length(csv_data(:,1));
plot(x, csv_data(:,1))
title(strcat("Control PID aplicado a el dut de", dut_translate{1}))
ylabel("Potencia (%)")
axis([0, length(csv_data(:,1)), 0, 110])
grid minor
P2 = subplot(2,1,2)
plot(x, csv_data(:,2))
ylabel("Temperatura (°C)")
axis([0, length(csv_data(:,1)), 0, 1000])
grid minor
print("-S1200,800", "pid_second_order_normal.png");
set(P1, 'YLim', [0, 110]);
set(P1, 'XLim', [400, 700]);
set(P2, 'YLim', [820, 900]);
set(P2, 'XLim', [400, 700]);
print("-S1200,800", "pid_second_order_zoom.png");

csv_data = dlmread(strcat(csv_directory, dut{1}, "WillStabilizeAt900DegreesWithProportionalControl.csv"), ",", 1, 0);
figure
P1 = subplot(2,1,1)
x = 1:length(csv_data(:,1));
plot(x, csv_data(:,1))
title(strcat("Control proporcional aplicado a el dut de", dut_translate{1}))
ylabel("Potencia (%)")
axis([0, length(csv_data(:,1)), 0, 110])
grid minor
P2 = subplot(2,1,2)
plot(x, csv_data(:,2))
ylabel("Temperatura (°C)")
axis([0, length(csv_data(:,1)), 0, 1000])
grid minor
print("-S1200,800", "proportional_second_order_normal.png");
set(P1, 'YLim', [0, 110]);
set(P1, 'XLim', [400, 600]);
set(P2, 'YLim', [800, 910]);
set(P2, 'XLim', [400, 600]);
print("-S1200,800", "proportional_second_order_zoom.png");

csv_data = dlmread(strcat(csv_directory, dut{1}, "WithHighTemperature.csv"), ",", 1, 0);
figure
P1 = subplot(2,1,1)
x = 1:length(csv_data(:,1));
plot(x, csv_data(:,1))
title(strcat("Control PID con temperatura inicial alta aplicado a el dut de", dut_translate{1}))
ylabel("Potencia (%)")
axis([0, length(csv_data(:,1)), 0, 110])
grid minor
P2 = subplot(2,1,2)
plot(x, csv_data(:,2))
ylabel("Temperatura (°C)")
axis([0, length(csv_data(:,1)), 0, 1000])
grid minor
print("-S1200,800", "high_temp_pid_second_order_normal.png");
set(P1, 'YLim', [0, 110]);
set(P1, 'XLim', [3000, 3800]);
set(P2, 'YLim', [0, 950]);
set(P2, 'XLim', [3000, 3800]);
print("-S1200,800", "high_temp_pid_second_order_zoom.png");

csv_data = dlmread(strcat(csv_directory, dut{1}, "Fuzzy2x3Control.csv"), ",", 1, 0);
figure
P1 = subplot(2,1,1)
x = 1:length(csv_data(:,1));
plot(x, csv_data(:,1))
title(strcat("Control Fuzzy 2x3 aplicado a el dut de", dut_translate{1}))
ylabel("Potencia (%)")
axis([0, length(csv_data(:,1)), 0, 110])
grid minor
P2 = subplot(2,1,2)
plot(x, csv_data(:,2))
ylabel("Temperatura (°C)")
axis([0, length(csv_data(:,1)), 0, 1000])
grid minor
print("-S1200,800", "fuzzy2x3_second_order_normal.png");
set(P1, 'YLim', [0, 110]);
set(P1, 'XLim', [500, 2300]);
set(P2, 'YLim', [860, 950]);
set(P2, 'XLim', [500, 2300]);
print("-S1200,800", "fuzzy2x3_second_order_zoom.png");

csv_data = dlmread(strcat(csv_directory, dut{1}, "Fuzzy2x1Control.csv"), ",", 1, 0);
figure
P1 = subplot(2,1,1)
x = 1:length(csv_data(:,1));
plot(x, csv_data(:,1))
title(strcat("Control Fuzzy2x1 aplicado a el dut de", dut_translate{1}))
ylabel("Potencia (%)")
axis([0, length(csv_data(:,1)), 0, 110])
grid minor
P2 = subplot(2,1,2)
plot(x, csv_data(:,2))
ylabel("Temperatura (°C)")
axis([0, length(csv_data(:,1)), 0, 1000])
grid minor
print("-S1200,800", "fuzzy2x1_second_order_normal.png");
set(P1, 'YLim', [0, 110]);
set(P1, 'XLim', [400, 1200]);
set(P2, 'YLim', [840, 920]);
set(P2, 'XLim', [400, 1200]);
print("-S1200,800", "fuzzy2x1_second_order_zoom.png");







csv_data = dlmread(strcat(csv_directory, dut{2}, "EndsWithMaxDegressIfWeAlwaysSetTo100PercentPower.csv"), ",", 1, 0);
figure
plot(csv_data(:, 2), '.');
title(strcat("Respuesta al escalon del DUT de ", dut_translate{1}))
grid minor
ylabel("Temperatura (°C)")
print("-S1200,800", "open_loop_first_order_normal.png");



csv_data = dlmread(strcat(csv_directory, dut{2}, "ZieglerNichols.csv"), ",", 1, 0);
figure
P1 = subplot(2,1,1)
x = 1:length(csv_data(:,1));
plot(x, csv_data(:,1))
title(strcat("Calculo de Ziegler Nichols para el dut de", dut_translate{2}))
ylabel("Potencia (%)")
axis([0, length(csv_data(:,1)), 0, 110])
grid minor
P2 = subplot(2,1,2)
plot(x, csv_data(:,2))
ylabel("Temperatura (°C)")
axis([0, length(csv_data(:,1)), 0, 2000])
grid minor
print("-S1200,800", "zn_first_order_normal.png");
set(P1, 'YLim', [0, 110]);
set(P1, 'XLim', [9530, 9540]);
set(P2, 'YLim', [610, 630]);
set(P2, 'XLim', [9530, 9540]);
print("-S1200,800", "zn_first_order_zoom.png");


csv_data = dlmread(strcat(csv_directory, dut{2}, "WillStabilizeAt900DegreesWithPIDControl.csv"), ",", 1, 0);
figure
P1 = subplot(2,1,1)
x = 1:length(csv_data(:,1));
plot(x, csv_data(:,1))
title(strcat("Control PID aplicado a el dut de", dut_translate{2}))
ylabel("Potencia (%)")
axis([0, length(csv_data(:,1)), 0, 110])
grid minor
P2 = subplot(2,1,2)
plot(x, csv_data(:,2))
ylabel("Temperatura (°C)")
axis([0, length(csv_data(:,1)), 0, 1000])
grid minor
print("-S1200,800", "pid_first_order_normal.png");
set(P1, 'YLim', [0, 110]);
set(P1, 'XLim', [1400, 1500]);
set(P2, 'YLim', [850, 950]);
set(P2, 'XLim', [1400, 1500]);
print("-S1200,800", "pid_first_order_zoom.png");

csv_data = dlmread(strcat(csv_directory, dut{2}, "WillStabilizeAt900DegreesWithProportionalControl.csv"), ",", 1, 0);
figure
P1 = subplot(2,1,1)
x = 1:length(csv_data(:,1));
plot(x, csv_data(:,1))
title(strcat("Control proporcional aplicado a el dut de", dut_translate{2}))
ylabel("Potencia (%)")
axis([0, length(csv_data(:,1)), 0, 110])
grid minor
P2 = subplot(2,1,2)
plot(x, csv_data(:,2))
ylabel("Temperatura (°C)")
axis([0, length(csv_data(:,1)), 0, 1000])
grid minor
print("-S1200,800", "proportional_first_order_normal.png");
set(P1, 'YLim', [0, 110]);
set(P1, 'XLim', [1400, 1700]);
set(P2, 'YLim', [850, 900]);
set(P2, 'XLim', [1400, 1700]);
print("-S1200,800", "proportional_first_order_zoom.png");

csv_data = dlmread(strcat(csv_directory, dut{2}, "WithHighTemperature.csv"), ",", 1, 0);
figure
P1 = subplot(2,1,1)
x = 1:length(csv_data(:,1));
plot(x, csv_data(:,1))
title(strcat("Control PID con temperatura inicial alta aplicado a el dut de", dut_translate{2}))
ylabel("Potencia (%)")
axis([0, length(csv_data(:,1)), 0, 110])
grid minor
P2 = subplot(2,1,2)
plot(x, csv_data(:,2))
ylabel("Temperatura (°C)")
axis([0, length(csv_data(:,1)), 0, 1000])
grid minor
print("-S1200,800", "pid_high_temp_first_order_normal.png");
set(P1, 'YLim', [0, 110]);
set(P1, 'XLim', [5700, 7000]);
set(P2, 'YLim', [300, 1000]);
set(P2, 'XLim', [5700, 7000]);
print("-S1200,800", "pid_high_temp_first_order_zoom.png");

csv_data = dlmread(strcat(csv_directory, dut{2}, "Fuzzy2x3Control.csv"), ",", 1, 0);
figure
P1 = subplot(2,1,1)
x = 1:length(csv_data(:,1));
plot(x, csv_data(:,1))
title(strcat("Control Fuzzy 2x3 aplicado a el dut de", dut_translate{2}))
ylabel("Potencia (%)")
axis([0, length(csv_data(:,1)), 0, 110])
grid minor
P2 = subplot(2,1,2)
plot(x, csv_data(:,2))
ylabel("Temperatura (°C)")
axis([0, length(csv_data(:,1)), 0, 1000])
grid minor
print("-S1200,800", "fuzzy2x3_first_order_normal.png");
set(P1, 'YLim', [0, 110]);
set(P1, 'XLim', [1500, 3200]);
set(P2, 'YLim', [860, 960]);
set(P2, 'XLim', [1500, 3200]);
print("-S1200,800", "fuzzy2x3_first_order_zoom.png");


csv_data = dlmread(strcat(csv_directory, dut{2}, "Fuzzy2x1Control.csv"), ",", 1, 0);
figure
P1 = subplot(2,1,1)
x = 1:length(csv_data(:,1));
plot(x, csv_data(:,1))
title(strcat("Control Fuzzy2x1 aplicado a el dut de", dut_translate{2}))
ylabel("Potencia (%)")
axis([0, length(csv_data(:,1)), 0, 110])
grid minor
P2 = subplot(2,1,2)
plot(x, csv_data(:,2))
ylabel("Temperatura (°C)")
axis([0, length(csv_data(:,1)), 0, 1000])
grid minor
print("-S1200,800", "fuzzy2x1_first_order_normal.png");
set(P1, 'YLim', [0, 110]);
set(P1, 'XLim', [1300, 3000]);
set(P2, 'YLim', [800, 1000]);
set(P2, 'XLim', [1300, 3000]);
print("-S1200,800", "fuzzy2x1_first_order_zoom.png");
