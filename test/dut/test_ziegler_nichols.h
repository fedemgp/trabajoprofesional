//
// Created by fedemgp on 29/11/20.
//

#ifndef TRABAJOPROFESIONAL_TEST_ZIEGLER_NICHOLS_H
#define TRABAJOPROFESIONAL_TEST_ZIEGLER_NICHOLS_H

#include <control/legacy_ziegler_nichols.h>

class TestZieglerNichols: public LegacyZieglerNichols {
public:
    TestZieglerNichols(int initial_power, int stationary_power, float cutoff_temp, float temp_sensitivity);
    legacy_zn_state_t getState() const;
    bool isRunning();
    void calculate(float &kp, float &kd, float &ki);
};


#endif //TRABAJOPROFESIONAL_TEST_ZIEGLER_NICHOLS_H
