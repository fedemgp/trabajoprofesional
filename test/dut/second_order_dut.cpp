/*
 * Created by fedemgp
 * Date: 1/5/21
 */

#include <gtest/gtest.h>
#include <control/fuzzy2x1.h>

#include "dut_emulator.h"
#include "csv_writter.h"
#include "control/control_algorithm.h"
#include "control/classic_pid.h"
#include "control/fuzzy2x3.h"
#include "test_ziegler_nichols.h"
#include "second_order_dut.h"

// Temperatura a partir de la cual la pendiente de temperatura cambia por efecto curie
#define CURIE_TEMPERATURE 700
#define SLOPE_UNDER_CURIE 0.0025
#define SLOPE_OVER_CURIE 0.0005
#define INERTIA 0.5
#define AMOUNT_OF_ITERATIONS 10000

float SecondOrderDut::process(float power) {
    float temp;
    float simulated_power;
    // Simulo el avance de la potencia para que no parezca que cambia de forma instantanea
    simulated_power = (power + INERTIA * (this->previous_power - power));
    // Este polinomio da la temperatura maxima dada una potencia,
    // we podria usar cualquier otra funcion
    temp = 1111.1 * simulated_power * simulated_power + 972.22 * simulated_power - 83.333;
    this->previous_power = simulated_power;
    // Ajusto la evolución de la temperatura en función si pasé el punto de curie o no.
    temp = temp - this->previous_temperature;
    if (previous_temperature < CURIE_TEMPERATURE)
        temp *= SLOPE_UNDER_CURIE;
    else
        temp *= SLOPE_OVER_CURIE;
    temp += this->previous_temperature;
    this->previous_temperature = temp;
    // TODO agregar ruido de medicion
    // n = 5*randn(1); % simula un ruido de medicion
    // ver https://en.cppreference.com/w/cpp/numeric/random/normal_distribution
    return temp;
}

SecondOrderDut::SecondOrderDut() {}
SecondOrderDut::SecondOrderDut(float init_temp, float init_power) : DutEmulator(init_temp, init_power) {}
SecondOrderDut::~SecondOrderDut() {}

TEST(ControlAlgorithms, SecondOrderDutReturns25DegreesIfThePowerIs10Percent) {
    SecondOrderDut dut;
    EXPECT_EQ(dut.process(0.1), 25.00);
}

TEST(ControlAlgorithms, SecondOrderDutEndsWithMaxDegressIfWeAlwaysSetTo100PercentPower) {
    SecondOrderDut dut;
    CsvWritter csv("SecondOrderDutEndsWithMaxDegressIfWeAlwaysSetTo100PercentPower.csv");
    float temp = dut.process(1);
    for (int i = 0; i < AMOUNT_OF_ITERATIONS; i++) {
        csv.writeData(1, temp);
        temp = dut.process(1);
    }
    ASSERT_NEAR(temp, 1990, 10);
}

TEST(ControlAlgorithms, SecondOrderDutWillStabilizeAt900DegreesWithProportionalControl) {
    SecondOrderDut dut;
    CsvWritter csv("SecondOrderDutWillStabilizeAt900DegreesWithProportionalControl.csv");
    ClassicPID pid(8, 0, 0, 900, 2.0f, 1);
    float temp = dut.process(0.1);
    for (int i = 0; i < AMOUNT_OF_ITERATIONS; i++) {
        float power = ControlAlgorithm::tapsToPower(pid._process(temp))/100;
        csv.writeData(power, temp);
        temp = dut.process(power);
    }
    ASSERT_NEAR(temp, 900, 10);
}

TEST(ControlAlgorithms, SecondOrderDutWillStabilizeAt900DegreesWithPIDControl) {
    SecondOrderDut dut;
    //ClassicPID pid(5, 0.05, 1, 900, 5.0f, 1);
    ClassicPID pid(5, 0.01, 0, 900, 5.0f, 1);
    CsvWritter csv("SecondOrderDutWillStabilizeAt900DegreesWithPIDControl.csv");
    float temp = dut.process(0.1);
    for (int i = 0; i < AMOUNT_OF_ITERATIONS; i++) {
        float power = ControlAlgorithm::tapsToPower(pid._process(temp))/100;
        csv.writeData(power, temp);
        temp = dut.process(power);
    }
    ASSERT_NEAR(temp, 900, 10);
}

TEST(ControlAlgorithms, SecondOrderDutFuzzy2x3Control) {
    SecondOrderDut dut;
    std::string filepath("./fuzzy/fuzzy2x3.json");
    Fuzzy2x3 fuzzy(900, 10.0f, 1, 0, 0.0001, filepath, 1, 900, 10, 10, 0.001, 0);
    CsvWritter csv("SecondOrderDutFuzzy2x3Control.csv");
    float temp = dut.process(0.1);
    for (int i = 0; i < AMOUNT_OF_ITERATIONS; i++) {
        float power = ControlAlgorithm::tapsToPower(fuzzy._process(temp))/100;
        csv.writeData(power, temp);
        temp = dut.process(power);
    }
    ASSERT_NEAR(temp, 900, 15);
}

TEST(ControlAlgorithms, SecondOrderDutWithHighTemperature) {
    SecondOrderDut dut(2000, 1);
    ClassicPID pid(5, 0.05, 0.1, 900, 5.0f, 1);
    CsvWritter csv("SecondOrderDutWithHighTemperature.csv");
    float temp = dut.process(1);
    for (int i = 0; i < AMOUNT_OF_ITERATIONS; i++) {
        float power = ControlAlgorithm::tapsToPower(pid._process(temp))/100;
        csv.writeData(power, temp);
        temp = dut.process(power);
    }
    ASSERT_NEAR(temp, 900, 10);
}

TEST(ControlAlgorithms, SecondOrderDutFuzzy2x1Control) {
    SecondOrderDut dut;
    std::string filepath("./fuzzy/fuzzy2x1.json");
    Fuzzy2x1 fuzzy(900, 10.0f, filepath, 1, 900, 10, 10);
    CsvWritter csv("SecondOrderDutFuzzy2x1Control.csv");
    float temp = dut.process(0.1);
    for (int i = 0; i < AMOUNT_OF_ITERATIONS; i++) {
        float power = ControlAlgorithm::tapsToPower(fuzzy._process(temp))/100;
        csv.writeData(power, temp);
        temp = dut.process(power);
    }
    ASSERT_NEAR(temp, 900, 15);
}

TEST(ControlAlgorithms, SecondOrderDutZieglerNichols) {
    SecondOrderDut dut;
    TestZieglerNichols zn(30, 70, 900, 0.5);
    CsvWritter csv("SecondOrderDutZieglerNichols.csv");
    float temp = dut.process(0.1);
    while (zn.isRunning()) {
        float power = ControlAlgorithm::tapsToPower(zn._process(temp))/100;
        if (zn.getState() != FINISHED) {
            csv.writeData(power, temp);
        }
        temp = dut.process(power);
    }
    float kp = 0;
    float kd = 0;
    float ki = 0;
    zn.calculate(kp, kd, ki);
    ASSERT_NEAR(kp, 1.51f, 0.01f);
    ASSERT_NEAR(ki, 0.04f, 0.01f);
    ASSERT_NEAR(kd, 0.00f, 0.01f);
}
