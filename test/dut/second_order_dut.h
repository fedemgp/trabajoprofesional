/*
 * Created by fedemgp
 * Date: 1/5/21
 */

#ifndef __SECOND_ORDER_DUT_H__
#define __SECOND_ORDER_DUT_H__


#include "dut_emulator.h"

class SecondOrderDut: public DutEmulator {
public:
    SecondOrderDut();
    SecondOrderDut(float init_temp, float init_power);
    virtual ~SecondOrderDut();
    float process(float power) override;
};


#endif // __SECOND_ORDER_DUT_H__