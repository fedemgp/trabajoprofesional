/**
 * Created by Federico Manuel Gomez Peter
 * Date: 17/10/20
 */
#include "dut_emulator.h"


DutEmulator::DutEmulator() {}
DutEmulator::DutEmulator(float init_temp, float init_power): previous_power(init_power), previous_temperature(init_temp) {}
DutEmulator::~DutEmulator() {}