/*
 * Created by fedemgp
 * Date: 1/5/21
 */

#ifndef __FIRST_ORDER_DUT_H__
#define __FIRST_ORDER_DUT_H__


#include "dut_emulator.h"

class FirstOrderDut: public DutEmulator {
public:
    FirstOrderDut();
    FirstOrderDut(float init_temp, float init_power);
    virtual ~FirstOrderDut();
    float process(float power) override;

};


#endif // __FIRST_ORDER_DUT_H__