//
// Created by fedemgp on 29/11/20.
//

#include "test_ziegler_nichols.h"

TestZieglerNichols::TestZieglerNichols(int initial_power, int stationary_power, float cutoff_temp, float temp_sensitivity):
        LegacyZieglerNichols(initial_power, stationary_power, cutoff_temp, temp_sensitivity) {}

legacy_zn_state_t TestZieglerNichols::getState() const {
    return this->state;
}

bool TestZieglerNichols::isRunning() {
    return this->keep_processing;
}

void TestZieglerNichols::calculate(float &kp, float &kd, float &ki) {
    this->calculateParameters();
    kp = this->kp;
    ki = this->ki;
    kd = this->kd;
}


