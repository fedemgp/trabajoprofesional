/*
 * Created by fedemgp
 * Date: 1/5/21
 */
#include <gtest/gtest.h>
#include <control/fuzzy2x1.h>

#include "first_order_dut.h"
#include "dut_emulator.h"
#include "csv_writter.h"
#include "control/control_algorithm.h"
#include "control/classic_pid.h"
#include "control/fuzzy2x3.h"
#include "test_ziegler_nichols.h"

#define AMOUNT_OF_ITERATIONS 10000


FirstOrderDut::FirstOrderDut() {}

FirstOrderDut::FirstOrderDut(float init_temp, float init_power) : DutEmulator(init_temp, init_power) {}

FirstOrderDut::~FirstOrderDut() {}

// Simulación de un sistema de primer orden de un DUT. Cumplirá la ecuación
// t[n] = 0.9996 * t[n-1] - 0.16 * p[n] + 0.168 * p[n-1]
// donde t[n] es la temperatura actual, t[n-1] es la temperatura anterior
// p[n] la potencia actual, p[n-1] la temperatura anterior
float FirstOrderDut::process(float power) {
    float temp = 0.9996 * this->previous_temperature - 16.0 * power + 16.8 * this->previous_power;
    this->previous_temperature = temp;
    this->previous_power = power;
    return temp;
}

TEST(ControlAlgorithms, FirstOrderDutReturns25DegreesIfThePowerIs10Percent) {
	FirstOrderDut dut(25, 0.1);
	EXPECT_NEAR(dut.process(0.1), 25, 0.1);
}

TEST(ControlAlgorithms, FirstOrderDutEndsWithMaxDegressIfWeAlwaysSetTo100PercentPower) {
    FirstOrderDut dut;
    CsvWritter csv("FirstOrderDutEndsWithMaxDegressIfWeAlwaysSetTo100PercentPower.csv");
    float temp = dut.process(1);
    for (int i = 0; i < AMOUNT_OF_ITERATIONS; i++) {
        csv.writeData(1, temp);
        temp = dut.process(1);
    }
    ASSERT_NEAR(temp, 1960, 10);
}

TEST(ControlAlgorithms, FirstOrderDutWillStabilizeAt900DegreesWithProportionalControl) {
	FirstOrderDut dut;
	CsvWritter csv("FirstOrderDutWillStabilizeAt900DegreesWithProportionalControl.csv");
	ClassicPID pid(5.0f, 0, 0, 900, 2.0f, 1);
	float temp = dut.process(0.1);
	for (int i = 0; i < AMOUNT_OF_ITERATIONS; i++) {
		float power = ControlAlgorithm::tapsToPower(pid._process(temp))/100;
		csv.writeData(power, temp);
		temp = dut.process(power);
	}
    ASSERT_NEAR(temp, 900, 10);
}

TEST(ControlAlgorithms, FirstOrderDutWillStabilizeAt900DegreesWithPIDControl) {
	FirstOrderDut dut;
//	ClassicPID pid(5, 0.005, 0, 900, 5.0f, 1);
    ClassicPID pid(3, 0.001, 0.1, 900, 5.0f, 1);
	CsvWritter csv("FirstOrderDutWillStabilizeAt900DegreesWithPIDControl.csv");
	float temp = dut.process(0.1);
	for (int i = 0; i < AMOUNT_OF_ITERATIONS; i++) {
		float power = ControlAlgorithm::tapsToPower(pid._process(temp))/100;
		csv.writeData(power, temp);
		temp = dut.process(power);
	}
    ASSERT_NEAR(temp, 900, 10);
}

TEST(ControlAlgorithms, FirstOrderDutFuzzy2x3Control) {
    FirstOrderDut dut;
    std::string filepath("./fuzzy/fuzzy2x3.json");
    Fuzzy2x3 fuzzy(900, 10.0f, 1, 0, 0.00005, filepath, 1, 900, 10, 0, 0.0001, 0);
    CsvWritter csv("FirstOrderDutFuzzy2x3Control.csv");
    float temp = dut.process(0.1);
    for (int i = 0; i < AMOUNT_OF_ITERATIONS; i++) {
        float power = ControlAlgorithm::tapsToPower(fuzzy._process(temp))/100;
        csv.writeData(power, temp);
        temp = dut.process(power);
    }
    ASSERT_NEAR(temp, 900, 15);
}

TEST(ControlAlgorithms, FirstOrderDutWithHighTemperature) {
	FirstOrderDut dut(2000, 1);
	ClassicPID pid(5, 0.05, 0.1, 900, 5.0f, 1);
	CsvWritter csv("FirstOrderDutWithHighTemperature.csv");
	float temp = dut.process(1);
	for (int i = 0; i < AMOUNT_OF_ITERATIONS; i++) {
	float power = ControlAlgorithm::tapsToPower(pid._process(temp))/100;
		csv.writeData(power, temp);
		temp = dut.process(power);
	}
    ASSERT_NEAR(temp, 900, 10);
}

TEST(ControlAlgorithms, FirstOrderDutFuzzy2x1Control) {
    FirstOrderDut dut;
    std::string filepath("./fuzzy/fuzzy2x1.json");
    Fuzzy2x1 fuzzy(900, 10.0f, filepath, 1, 900, 10, 10);
    CsvWritter csv("FirstOrderDutFuzzy2x1Control.csv");
    float temp = dut.process(0.1);
    for (int i = 0; i < AMOUNT_OF_ITERATIONS; i++) {
        float power = ControlAlgorithm::tapsToPower(fuzzy._process(temp))/100;
        csv.writeData(power, temp);
        temp = dut.process(power);
    }
    ASSERT_NEAR(temp, 900, 15);
}


TEST(ControlAlgorithms, FirstOrderDutZieglerNichols) {
	FirstOrderDut dut;
	TestZieglerNichols zn(30, 70, 900, 0.1);
	CsvWritter csv("FirstOrderDutZieglerNichols.csv");
	float temp = dut.process(10);
	while (zn.isRunning()) {
		float power = ControlAlgorithm::tapsToPower(zn._process(temp))/100;
        if (zn.getState() != FINISHED){
            csv.writeData(power, temp);
		}
		temp = dut.process(power);
	}
	float kp = 0;
	float kd = 0;
	float ki = 0;
	zn.calculate(kp, kd, ki);
	ASSERT_NEAR(kp, 38.38f, 0.01f);
	ASSERT_NEAR(ki, 4.26f, 0.01f);
	ASSERT_NEAR(kd, 0.00f, 0.01f);
}
