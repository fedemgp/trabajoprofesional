#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <QWidget>
#include <QSerialPort>
#include <QTimer>
#include <QByteArray>
#include <memory>
namespace Ui {
class main_window;
}

typedef enum {WAITING2, READING2} reading_status_t2;

class main_window : public QWidget
{
    Q_OBJECT

public:
    explicit main_window(QWidget *parent = nullptr);
    ~main_window();

private slots:
    void on_connect_clicked();
    void on_spam_clicked();
    void sendPower();
    void ready_read();
    void handle_error(QSerialPort::SerialPortError);

private:
    Ui::main_window *ui;
    QSerialPort *serial;
    QTimer *timer;
    reading_status_t2  reading_status = WAITING2;
    uint32_t bytes_arrived_count = 0;
    QByteArray bytes_arrived;

    void process_message();
};

#endif // MAIN_WINDOW_H
