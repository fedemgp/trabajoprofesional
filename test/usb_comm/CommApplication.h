/*
 * Created by fedemgp
 * Date: 15/4/21
 */

#ifndef __COMMAPPLICATION_H__
#define __COMMAPPLICATION_H__


#include <QEvent>
#include <QApplication>

class CommApplication: public QApplication {
public:
    CommApplication(int& argc, char** argv);
    bool notify(QObject* receiver, QEvent* event);
};


#endif // __COMMAPPLICATION_H__