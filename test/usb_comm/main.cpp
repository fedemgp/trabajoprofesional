/*
 * Created by fedemgp
 * Date: 15/4/21
 */
/**
 * Este ejecutable se usará para testear el microcontrolador, la idea es tener una interfaz usb simple, sin
 * nada de GUI de QT, solamente el QSerialport. Con esto vamos a testear la comunicación con el microcontrolador
 * y corroborar que este responde acorde a lo esperado.
 */
#include <logger/logger.h>
#include "CommApplication.h"
#include "main_window.h"

int main(int argc, char *argv[]) {
    CommApplication application(argc, argv);
    Logger::init();
    main_window window;
    window.show();
    return application.exec();
}

