#include <logger/logger.h>
#include <QSerialPortInfo>
#include <QBEInteger>
#include "main_window.h"
#include "ui_main_window.h"

main_window::main_window(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::main_window),
    bytes_arrived(8,0)
{
    ui->setupUi(this);
    this->serial = new QSerialPort(this);
    this->timer = new QTimer(this);
    connect(this->serial, &QSerialPort::errorOccurred, this, &main_window::handle_error);
    connect(this->serial, &QSerialPort::readyRead, this, &main_window::ready_read);
    connect(this->timer, &QTimer::timeout, this, &main_window::sendPower);
}

main_window::~main_window()
{
    delete ui;
}

void main_window::on_connect_clicked()
{
    auto portsInfo = QSerialPortInfo::availablePorts();
    for(auto &info : portsInfo){
        if(info.serialNumber() == "12345679"){
            this->serial->setPort(info);
            this->serial->setBaudRate(115200);
            this->serial->setDataBits(QSerialPort::Data8);
            this->serial->setParity(QSerialPort::NoParity);
            if(this->serial->open(QIODevice::ReadWrite)){
                Logger::info("Puerto serie conectado en puerto %s", info.portName().toStdString().c_str());
                return;
            } else {
                Logger::warning("No se pudo conectar al puerto %s", info.portName().toStdString().c_str());
            }
        }
    }
    Logger::warning("Terminé de iterar los puertos disponibles y no pude conectarme");
}

void main_window::on_spam_clicked()
{
    this->timer->start(this->ui->timeoutLEdit->text().toInt());
}

void main_window::sendPower() {
    QByteArray ret(8, 0x00);
    ret[0] = 0x7E;
    ret[1] = 0x20;
    ret[2] = 0x00;
    this->serial->write(ret);
}

void main_window::handle_error(QSerialPort::SerialPortError error) {
    if (error == 0)
        return;
    Logger::warning("Error en el puerto serie, codigo: %i (%s)",
                    (int) error,
                    this->serial->errorString().toStdString().c_str());
    // TODO: tal vez debería intentar reconectarse siempre.

}

void main_window::ready_read() {
    QByteArray readArray = this->serial->readAll();
    for(auto readByte : readArray){
        switch(reading_status){
            case WAITING2:
                if(readByte == 0x7E){
                    bytes_arrived[bytes_arrived_count] = readByte;
                    bytes_arrived_count++;
                    reading_status = READING2;
                }
                break;
            case READING2:
                bytes_arrived[bytes_arrived_count] = readByte;
                bytes_arrived_count++;
                if (readByte == 0x7E) {
                    bytes_arrived_count = 0;
                    bytes_arrived[bytes_arrived_count] = readByte;
                    bytes_arrived_count++;
                } else if(bytes_arrived_count == 8){
                    reading_status = WAITING2;
                    bytes_arrived_count = 0;
                    process_message();
                }
                break;
        }
    }
}
#define OFFSET 8
#define RESERVED_BITS 5
#define VALUE_MASK 0x3ffff
#define SIGN_POSITION 18
#define DIVISOR 128.0
void main_window::process_message() {
    switch (bytes_arrived[1]) {
        // Power set ack
        case 0x21: {
            float power = 100.0f * (127.0f - bytes_arrived[2]) / 127.0f;
            Logger::debug("Power set ack (%.2f%%)", power);
            break;
        }
        // temperature reading
        case 0x10: {
            uint32_t dataBits = qFromBigEndian(*(uint32_t *) &bytes_arrived.data()[2]);
            float temp = ((dataBits >> OFFSET) >> RESERVED_BITS) & VALUE_MASK;
            int sign = 1 - 2 * ((((dataBits >> OFFSET) >> RESERVED_BITS)
                                 & (1 << SIGN_POSITION))>>SIGN_POSITION);
            temp = (float) (sign * temp / DIVISOR);
            this->ui->label->setText(QString::number(temp));
          //  Logger::debug("Temperature reading: %.2f", temp);
            break;
        }
        // cold junctions temp
        case 0x11:{
            uint32_t dataBits = qFromBigEndian(*(uint32_t *) &bytes_arrived.data()[2]);
            float temp = ((dataBits >> OFFSET) >> RESERVED_BITS) & VALUE_MASK;
            int sign = 1 - 2 * ((((dataBits >> OFFSET) >> RESERVED_BITS)
                                 & (1 << SIGN_POSITION))>>SIGN_POSITION);
            temp = (float) (sign * temp / DIVISOR);
            this->ui->coldj_temp->setText(QString::number(temp));
            //Logger::debug("Cold junction temperature reading: %.2f", temp);
            break;
        }
        default:
            break;
    }
}
