/*
 * Created by fedemgp
 * Date: 15/4/21
 */

#include <messages.h>
#include <logger/logger.h>
#include "CommApplication.h"
/*
run() {
    SerialPort serial(nullptr);
    serial.findDevice();

    std::shared_ptr<MicroMessage> msg(new SetPower(0));
    while(true) {
        serial.send(msg);
        usleep(1000000);
    }
}*/

bool CommApplication::notify(QObject *receiver, QEvent *event) {
    try {
        return QApplication::notify(receiver, event);
    } catch (std::exception &e) {
        Logger::critical(e.what());
        this->exit(1);
    } catch (...) {
        Logger::critical(UNKNOWN_ERROR_MSG, "CommApplication");
        this->exit(1);
    }
    return false;
}

CommApplication::CommApplication(int &argc, char **argv): QApplication(argc, argv) {}
